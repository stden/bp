{ ������������ ������� ������������� ��������� }
program Lab5;

uses Corout_5, CRT, DOS;

Var
  proc1, proc2, proc3: descptr;
  Sem: TSemaphore;
  L1: longint;
  Count: word;
  time: pointer;
  fil: text;

Const
  N = 10;
  T = 5;

{-----------------------------------------------------}
Procedure P1;
Var i: integer;
Begin
  while true do begin
  {Sem.P;}
      for i:=1 to N do
        begin
          Write(fil,'1');
          delayP(T);
        end;{for 1..N}
  {Sem.V;}

  end; {while}
End {P1};
{-----------------------------------------------------}
Procedure P2;
Var j: integer;
Begin
  while true do begin
  {Sem.P;}
      for j:=1 to N do
        begin
          Write(fil,'2');
          delayP(T);
        end;{for 1..N}
  {Sem.V;}
  end;{while}
End {P2};
Procedure P3;
Var
     i : LongInt;
     C : char;
Begin
     i := 0;
     while true do begin
          {writeln('proc3');}
      {    writeln('Step: ', i);}
          if KeyPressed then
            if Readkey=#27 then begin
               Sem.Done;
               StopDisp;
            end;
      end {while};
End {P3};
Begin
  Count:=1;
  Sem.Init(1);
  L1:=MemAvail;
  ClrScr;
  Assign(fil,'\fil.txt');
  Rewrite(fil);
  CreateProcess(@P1, proc1);
  CreateProcess(@P2, proc2);
  CreateProcess(@P3, proc3);
  StartDisp;
  WriteLn('��窠 = ', L1-MemAvail, ' ����.');
  Close(fil);
End.