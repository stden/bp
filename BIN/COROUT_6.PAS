Unit Corout_6;

Interface

uses OBJECTS, DOS;

const Num=10;

Type
    arptr    = ^artype;
    artype   = array[0..999] of word; {��� �⥪ - 1000 ᫮�}

    PDescriptor  = ^procdesc;
    procdesc = record
                     ssreg,
                     spreg  : word;
                     StackAdr: arptr;
                     list: pCollection;
                     tact: longint;

               end; {record}
    PSemaphore = ^TSemaphore;
    TSemaphore = object
                       counter: integer;
                       list: TCollection;
                       Constructor Init(C: integer);
                       Destructor Done; Virtual;
                       Procedure P;
                       Procedure V;
                 end; {TSemaphore}
    TBuffer = Object
                    input, output : word;
                    n: word;
                    Buf: Array[0..Num-1] Of word;
                    ReadList, WriteList: TCollection;
                    Constructor Init;
                    Destructor Done; Virtual;
                    Procedure WriteBuf(M: word);
                    Procedure ReadBuf(Var M: word);
                    Procedure WaitRead;
                    Procedure SignalRead;
                    Procedure WaitWrite;
                    Procedure SignalWrite;
                 end; {TBuffer}

Procedure StopProcess(proc: PDescriptor);
Procedure RestartProcess(proc: PDescriptor);
Procedure DelayP(Delay: longint);

Procedure StopDisp;
Procedure CreateProcess(body: pointer; Var proc: PDescriptor);
Procedure KillProcess(proc: PDescriptor);
Procedure SelfKill;
Procedure StartDisp;

{--------------------------------------------------------------------------}
Implementation

Var
   T: longint;
   time: pointer;
   main, proc1, proc2, proc3: PDescriptor;
   ReadyList:TCollection;
   StopList:TCollection;
   StopTList:TCollection;
   KillList:TCollection;
   Curproc: PDescriptor;
{--------------------------------------------------------------------------}

procedure EnableInterrupt;
begin
  asm
    sti
  end;
end; {EnableInterrupt}
{--------------------------------------------------------------------------}

Procedure BanInterrupt;
begin
  Asm
    Cli
  end; {Asm}
end; {BanInterrupt}
{--------------------------------------------------------------------------}

Procedure StopProcess(proc:PDescriptor);
begin
  BanInterrupt;
  ReadyList.Delete(proc);
  proc^.list:=nil;
  StopList.Insert(proc);
  proc^.list:=@StopList;
  EnableInterrupt;
end; {StopProcess}
{--------------------------------------------------------------------------}

Procedure RestartProcess(proc:PDescriptor);
var List: pCollection;
begin
  BanInterrupt;
  List:= proc^.List;
  list^.Delete(proc);
  proc^.list:= nil;
  ReadyList.Insert(proc);
  proc^.list:= @ReadyList;
  EnableInterrupt;
end; {RestartProcess}
{--------------------------------------------------------------------------}
Procedure Activisation;
Var cr: PDescriptor;
    i, j: integer;
begin
  BanInterrupt;
  if (StopTList.count <> 0) then
  begin
    i := 0;
    while (i <= (StopTList.count -1) ) do
      begin
      cr := StopTList.at(i);
        {Writeln('Tact= ',cr^.Tact);}
        if cr^.Tact = T then
          begin
            StopTList.Delete(cr);
            cr^.list:= nil;
            ReadyList.Insert(cr);
            cr^.list:= @ReadyList;
         end
        else
          Inc(i);
      end;
  end;
  EnableInterrupt;
end; {Activisation}
{--------------------------------------------------------------------------}

Procedure NewProcess(body : pointer; Var proc : PDescriptor);
Var
     ar : arptr;
Begin
     New(proc);
     New(ar);

     with proc^ do
     begin
          ssreg := seg(ar^);
          spreg := ofs(ar^) + 1998 - 14;
          StackAdr := ar;
          memw[ssreg:spreg+2] := ofs(body^);
          memw[ssreg:spreg+4] := seg(body^);
     end {with};
End {NewProcess};
{--------------------------------------------------------------------------}

Procedure CreateProcess(body:pointer; Var proc:PDescriptor);
begin
  BanInterrupt;
  NewProcess(body, proc);
  ReadyList.Insert(proc);
  proc^.List:= @ReadyList;
  EnableInterrupt;
end; {CreateProcess}
{-------------------------------------------------------}

Procedure KillProcess(proc:PDescriptor);
Var
  List:pCollection;
begin
  BanInterrupt;
  List:= proc^.List;
  if List <> nil then
    List^.Delete(proc);
  proc^.list:= nil;
  KillList.Insert(proc);
  EnableInterrupt;
end; {KillProcess}
{-------------------------------------------------------}
{--------------------------------------------------------------------------}
Procedure ClearKillList;
var KillProc: PDescriptor;
begin
  while KillList.Count <> 0 do
    begin
      KillProc:= KillList.At(0);
      KillList.AtDelete(0);
      Dispose(KillProc^.StackAdr);
      Dispose(KillProc);
    end;
end; {ClearKillList}
{--------------------------------------------------------------------------}

Procedure Transfer(OldProc, NewProc : PDescriptor);
  Assembler;
  Asm                    {��������� ��᫥ Call Transfer
                        ����⠢��� push bp; mov bp,sp}
   les di,oldproc
   mov es:[di],ss      {oldproc.ssreg := ss;}
   mov es:[di+2],sp    {oldproc.spreg := sp; ���� ������ � sp+2}
   les di,newproc
   mov ss,es:[di]      {ss := newproc.ssreg;}
   mov sp,es:[di+2]    {sp := newproc.spreg;}
   pop bp      {��⠫������� bp �뢮��� �⥪ �� ���� ������}
   sti
   ret 8
       {��⮫��㫨 8 ���⮢ - 4 ᫮�� - ���祭�� oldproc � newproc}
End {Transfer};
{-----------------------------------------------------}

{$F+}
Procedure Handler; interrupt;
  var
    OldProc:PDescriptor;
  begin
    Inc(T);
    BanInterrupt;
    ClearKillList;
    Activisation;
    Port[$20]:= $20;
    OldProc:= CurProc;
    ReadyList.Insert(CurProc);
    CurProc^.list:= @ReadyList;
    CurProc:= ReadyList.at(0);
    ReadyList.atDelete(0);
    CurProc^.list:= nil;
    Transfer(OldProc, CurProc);
  end; {Handler}
{$F-}

{-----------------------------------------------------}
Procedure SelfKill;
var KillProc: PDescriptor;
begin
  BanInterrupt;
  KillProc:= CurProc;
  CurProc:= ReadyList.At(0);
  ReadyList.atDelete(0);
  CurProc^.list:= nil;
  KillProcess(KillProc);
  Transfer(KillProc, CurProc);
end; {SelfKill}
{-----------------------------------------------------}
Procedure DelayP(Delay: longint);
Var OldProc: PDescriptor;
begin
  BanInterrupt;
  OldProc:= CurProc;
  OldProc^.Tact:= T + Delay;
  StopTList.Insert(OldProc);
  OldProc^.list:= @StopTList;
  CurProc:= ReadyList.at(0);
  CurProc^.list:= nil;
  ReadyList.atDelete(0);
  Transfer(OldProc, CurProc);
end; {DelayP}
{--------------------------------------------------------------------------}


Procedure StartDisp;
begin
  BanInterrupt;
  GetIntVec($1C,time);
  SetIntVec($1C,addr(Handler));
  T:= 0;
  CurProc:=ReadyList.at(0);
  ReadyList.atDelete(0);
  CurProc^.list:= nil;
  Transfer(main, CurProc);
end; {StartDisp}
{-------------------------------------------------------}

Procedure StopDisp;
var KillProc, pproc: PDescriptor;
begin
  BanInterrupt;
  while (StopTList.count <> 0) do
  begin
    ReadyList.Insert(StopTList.At(0));
    StopTList.AtDelete(0);
  end;
  ReadyList.Insert(CurProc);
  pproc:= CurProc;
  while ReadyList.Count <> 0 do
    begin
      KillProc:= ReadyList.At(0);
      ReadyList.AtDelete(0);
      Dispose(KillProc^.StackAdr);
      Dispose(KillProc);
    end;
  SetIntVec($1C, time);
  Transfer(pproc, main);
end; {StopDisp}
{-------------------------------------------------------}
Constructor TSemaphore.Init(c: integer);
begin
  Counter:= C;
  list.Init(10,5);
end;{TSemaphore.Init}
{-------------------------------------------------------}
Destructor TSemaphore.Done;
begin
  list.FreeAll;
end; {TSemaphore.Done}
{-------------------------------------------------------}
Procedure TSemaphore.P;
Var
  OldProc: PDescriptor;
begin
  BanInterrupt;
  Dec(Counter);
  if Counter < 0 then
    begin
      OldProc:= CurProc;
      list.Insert(OldProc);
      CurProc:= ReadyList.At(0);
      ReadyList.AtDelete(0);
      Transfer(OldProc, CurProc);
    end; {if}
  EnableInterrupt;
end; {TSemaphore.P}
{-------------------------------------------------------}
Procedure TSemaphore.V;
Var
  OldProc: PDescriptor;
begin
  BanInterrupt;
  Inc(Counter);
  if Counter <= 0 then
    begin
      OldProc:= CurProc;
      ReadyList.Insert(OldProc);
      CurProc:= list.At(0);
      list.AtDelete(0);
      Transfer(OldProc, CurProc);
    end; {if}
  EnableInterrupt;
end; {TSemaphore.V}
{-------------------------------------------------------}
Constructor TBuffer.Init;
begin
  input:= 0;
  output:= 0;
  n:=0;
  ReadList.Init(10,5);
  WriteList.Init(10,5);
end; {TBuffer.Init}
{-------------------------------------------------------}
Destructor TBuffer.Done;
Var KillProc: PDescriptor;
begin
  while ReadList.Count <> 0 do
    begin
      KillProc:= ReadList.At(0);
      ReadList.AtDelete(0);
      Dispose(KillProc^.StackAdr);
      Dispose(KillProc);
    end;
  while WriteList.Count <> 0 do
    begin
      KillProc:= WriteList.At(0);
      WriteList.AtDelete(0);
      Dispose(KillProc^.StackAdr);
      Dispose(KillProc);
    end;
end; {TBuffer.Done}
{-------------------------------------------------------}
Procedure TBuffer.WaitRead;
var OldProc: PDescriptor;
begin
  OldProc:= CurProc;
  ReadList.Insert(OldProc);
  if ReadyList.Count<>0 then
    begin
      CurProc:= ReadyList.At(0);
      ReadyList.AtDelete(0);
    end{then}
  else
    begin
      CurProc:= WriteList.At(0);
      WriteList.AtDelete(0);
    end;{if}
  Transfer(OldProc, CurProc);
end; {TBuffer.WaitRead}
{-------------------------------------------------------}
Procedure TBuffer.WaitWrite;
var OldProc: PDescriptor;
begin
  OldProc:= CurProc;
  WriteList.Insert(OldProc);
  if ReadyList.Count<>0 then
    begin
      CurProc:= ReadyList.At(0);
      ReadyList.AtDelete(0);
    end{then}
  else
    begin
      CurProc:= ReadList.At(0);
      ReadList.AtDelete(0);
    end;{if}
  Transfer(OldProc, CurProc);
end;{TBuffer.WaitWrite}
{-------------------------------------------------------}
Procedure TBuffer.SignalRead;
var OldProc, Local: PDescriptor;
begin
  local:= nil;
  if ReadList.count<>0 then
    Local:= ReadList.At(0);
  if Local <> nil then
    begin
      OldProc:= CurProc;
      CurProc:= Local;
      ReadList.Delete(Local);
      ReadyList.Insert(OldProc);
      Transfer(OldProc, CurProc);
    end {if}
end;{TBuffer.SignalRead}
{-------------------------------------------------------}
Procedure TBuffer.SignalWrite;
var OldProc, Local: PDescriptor;
begin
  Local:= nil;
  if WriteList.Count<>0 then
    Local:= WriteList.At(0);
  if Local <> nil then
    begin
      OldProc:= CurProc;
      CurProc:= Local;
      WriteList.Delete(Local);
      ReadyList.Insert(OldProc);
      Transfer(OldProc, CurProc);
    end {if}
end;{TBuffer.SignalWrite}
{-------------------------------------------------------}
Procedure TBuffer.WriteBuf(M: word);
begin
  BanInterrupt;
  Activisation;
  if n = Num then
    WaitWrite;
  Inc(n);
  Buf[input]:= M;
  Writeln('Writen');
  input:= (input + 1) MOD Num;
  SignalRead;
  EnableInterrupt
end; {TBuffer.Write}
{-------------------------------------------------------}
Procedure TBuffer.ReadBuf(Var M: word);
begin
  BanInterrupt;
  Activisation;
  if (n=0) then
    WaitRead;
  Dec(n);
  M:= Buf[output];
  WriteLn('  Readen');
  output:= (output + 1) MOD Num;
  SignalWrite;
  EnableInterrupt;
end; {TBuffer.Read}
{-------------------------------------------------------}
begin
  New(main);
  ReadyList.Init(10,5);
  StopList.Init(10,5);
  StopTList.Init(10,5);
  KillList.Init(10,5);
end. {Corout}