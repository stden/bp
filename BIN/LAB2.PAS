{ ������������ ��������� ��������������� ��������� }
program Lab2;

uses Corout_2, CRT, DOS;

var
  proc1, proc2, proc3 : PDescriptor;
  Count : word;
  time : pointer;

{ ��ࠡ��稪 ���뢠��� ⠩��� }
{$F+}
Procedure Handler; interrupt;
begin
  DenyInterrupt; { ����頥� ���뢠��� }

  Port[$20] := $20; 

  case Count of
    1: begin
      Count := 2;
      Transfer(proc1, proc2);
    end;
    2: begin
      Count := 3;
      Transfer(proc2, proc3);
    end;
    3: begin
      Count := 1;
      Transfer(proc3, proc1);
    end;
  end;

  AllowInterrupt;
end; {Handler}
{$F-}

procedure StartAll;
begin
  DenyInterrupt;

  { �� ������ "⨪�" ⠩��� (�ਬ�୮ 18.2 ࠧ� � ᥪ㭤�) ��뢠���� ���뢠���� 1�h }
  GetIntVec($1C,time); { ���������� ��ࠡ��稪 ���뢠��� }
  SetIntVec($1C,addr(Handler));

  Transfer(main, proc1);
end;

procedure StopAll;
begin
  DenyInterrupt;
  SetIntVec($1C, time);
  Transfer(proc3, main);
end;

{------------------- ----------------------------------}
Procedure P1;
Begin
  while true do begin
    writeln('proc1');
  end {while};
End {P1};

{-----------------------------------------------------}
Procedure P2;
Begin
  while true do begin
    writeln('proc2');
  end {while};
End {P2};
{-----------------------------------------------------}
Procedure P3;
Begin
  while true do begin
    writeln('proc3');
    if KeyPressed then
      if Readkey=#27 then begin
        StopAll;
      end;
  end {while};
End {P3};
{-----------------------------------------------------}

var MemStart : LongInt;
begin
  Count:=1;
  MemStart := MemAvail;
  ClrScr;

  NewProcess(@p1, proc1);
  NewProcess(@p2, proc2);
  NewProcess(@p3, proc3);

  StartAll;

  KillProcess(proc3);
  KillProcess(proc2);
  KillProcess(proc1);
  
  WriteLn('��窠 = ', MemStart - MemAvail, ' ����.');
end.